CREATE DATABASE number CHARACTER SET utf8 COLLATE utf8_general_ci;

grant all on numbers.* to 'db_user'@'%' identified by 'db_password';
