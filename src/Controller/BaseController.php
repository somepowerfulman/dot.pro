<?php

namespace App\Controller;


use App\Views\BaseView;

class BaseController
{
    protected $dbConnection;
    protected $requestMethod;
    protected $viewName;

    public function __construct($dbConnection, $requestMethod, $viewName)
    {
        $this->dbConnection = $dbConnection;
        $this->requestMethod = $requestMethod;
        $this->viewName = $viewName;
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                $response = $this->getCountries();
                break;
            case 'POST':
                $response = $this->createItem();
                break;
        }
    }

}
//insert into