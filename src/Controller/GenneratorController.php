<?php


namespace App\Controller;


use App\Views\BaseView;

class GenneratorController extends BaseController
{


    protected function getCountries()
    {
//        "SELECT count(*) FROM numbers.phone_number;";
//        $sql = "SELECT phone_code.country, phone_code.code, count(phone_number.number) as amount FROM numbers.phone_code join phone_number on phone_code.id = phone_number.code_id group by phone_code.country, phone_code.code;";


                $sql = "SELECT * FROM numbers.phone_code";
//        $sql = "SELECT * FROM numbers.phone_code join numbers.phone_number on phone_code.id = phone_number.code_id";
        try {
            $statement = $this->dbConnection->getConnection()->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            new BaseView($result, $this->viewName);

//    var_dump($result);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }


//    }

    protected function createItem()
    {
//        header( 'Refresh: 1; url=/statistic' );

        if ($_POST['count'] % count($_POST['multiple']) == 0) {
            $this->generate($_POST['count'], count($_POST['multiple']));
        }else{
            $this->generate($_POST['count']-1, count($_POST['multiple']));
        }

    }

    protected function insertItemToDB($code, $number)
    {

        $sql = "insert into numbers.phone_number (`number`, `code_id`) values (:number, :code)";
        try {
            $statement = $this->dbConnection->getConnection()->prepare($sql);
            $statement->bindParam(':number', $number);
            $statement->bindParam(':code', $code);
            $statement->execute();


//    var_dump($statement);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

    }

    protected function generate($amount, $range)
    {
        $count_generation = [];
        $count = 0;
        for ($i = 0; $i < $amount / $range; $i++) {
            for ($j = 0; $j < count($_POST['multiple']); $j++) {
                $random = mt_rand();
                $count++;
                $count_generation = [$_POST['multiple'][$j] => ["$count"]];
                $this->insertItemToDB($_POST['multiple'][$j], $random);
            }
        }
        header("Location: statistic");
        exit();
    }
}