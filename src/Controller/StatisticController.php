<?php


namespace App\Controller;


use App\Views\BaseView;
use App\Views\StatisticView;

class StatisticController extends BaseController
{
    protected $dbConnection;
    protected $requestMethod;
    protected $viewName;

    public function __construct($dbConnection, $requestMethod, $viewName)
    {
        $this->dbConnection = $dbConnection;
        $this->requestMethod = $requestMethod;
        $this->viewName = $viewName;
    }
    public function processRequest()
    {

                $response = $this->getStatistic();

    }

public function getStatistic()
{
    $sql_count = "SELECT  phone_code.code, phone_code.country, COUNT(phone_number.number) AS amount FROM numbers.phone_code
LEFT JOIN numbers.phone_number ON phone_code.id = phone_number.code_id
GROUP BY phone_code.code, phone_code.country order by amount desc";
    $sql_max = "SELECT  phone_code.code, phone_code.country, COUNT(phone_number.number) AS amount FROM numbers.phone_code
LEFT JOIN numbers.phone_number ON phone_code.id = phone_number.code_id
GROUP BY phone_code.code, phone_code.country order by amount desc limit 1;";
    $sql_min = "SELECT  phone_code.code, phone_code.country, COUNT(phone_number.number) AS amount FROM numbers.phone_code
LEFT JOIN numbers.phone_number ON phone_code.id = phone_number.code_id
GROUP BY phone_code.code, phone_code.country order by amount asc limit 1;";
    try {
        $result['all'] = $this->prepareGet($sql_count);
        $result['max'] = $this->prepareGet($sql_max);
        $result['min'] = $this->prepareGet($sql_min);
        new BaseView($result, $this->viewName);
    } catch (\PDOException $e) {
        exit($e->getMessage());
    }
}
public function prepareGet($sql)
{
    $statement = $this->dbConnection->getConnection()->prepare($sql);
    $statement->execute();
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
}
}