<?php


namespace App\System;


class DatabaseConnector
{
    private $dbConnection = null;

    public function __construct()
    {
        try {
            $this->dbConnection = new \PDO(
                "mysql:host=db;
        port = 3306;charset=utf8mb4;
        dbname = numbers",
                'db_user',
                'db_password');
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function getConnection(){
        return $this->dbConnection;
    }
}