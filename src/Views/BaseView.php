<?php


namespace App\Views;


class BaseView
{
    private $path;
    private $layout = 'default';
    public function __construct($data, $viewName)
    {
        $this->path = dirname(__DIR__).'/View/template/';
        $this->layout = $viewName.'.php';
        include dirname(__DIR__).'/Views/templates/header.php';
        include (dirname(__DIR__).'/Views/templates/'.$viewName.'.php');
        include dirname(__DIR__).'/Views/templates/footer.php';
    }

    public function render($title, $data)
    {

    }

}