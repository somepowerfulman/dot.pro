        </div>
    </div>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">


    <script type="text/javascript" src="https://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
    <!--    <script type="text/javascript" src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>-->
    <script>
        $(document).on('click', '.btn-add', function (event) {
            event.preventDefault();
            var field = $(this).closest('.form-group');
            var field_new = field.clone();

            $(this)
                .toggleClass('btn-success')
                .toggleClass('btn-add')
                .toggleClass('btn-danger')
                .toggleClass('btn-remove')
                .html('✖');

            field_new.find('input').val('');
            field_new.insertAfter(field);
        });

        $(document).on('click', '.btn-remove', function (event) {
            event.preventDefault();
            $(this).closest('.form-group').remove();
        });

    </script>
    </body>
</html>