<table class="mt-5 table table-bordered">
    <thead>
    <tr>
        <th>code</th>
        <th>Country</th>
        <th>amo</th>
    </tr>
    </thead>

    <?php
    $total_amount = 0;
    foreach ($data['all'] as $item) {
        $total_amount += $item['amount'];
        ?>
        <tbody>
        <tr>
            <td><?= $item['code'] ?></td>
            <td><?= $item['country'] ?></td>
            <td><?= $item['amount'] ?></td>
        </tr>

        </tbody>
        <?php

    }

//    var_dump($data['max']);
    ?>
</table>
<table class="mt-5 table table-bordered">
    <thead>
    <tr>
        <th>ІТОГО</th>

    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Максимум</td>
        <td><?= $data['max'][0]['country'] ?></td>
        <td><?= $data['max'][0]['amount'] ?></td>
    </tr>
    <tr>
        <td>Мінімум</td>
        <td><?= $data['min'][0]['country'] ?></td>
        <td><?= $data['min'][0]['amount'] ?></td>
    </tr>
    <tr>
        <td>Загальна кількість номерів</td>
        <td></td>
        <td><?= $total_amount ?></td>
    </tr>
    </tbody>
</table>

<hr><br>
<a class="btn btn-block btn-primary col-3" href="/">Головна</a>
